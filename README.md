# CigSsa.db

`CigSsa.db` is an R package that provides annotations for the Atlantic
salmon genome (latest CIGENE genome build: cigene3.6\_chrom)

## Install/update CigSsa.db

You can track (and contribute to) development of `CigSsa.db` at [GitLab](https://gitlab.com/Cigene/CigSsa.db) To install it:

1. Install the release version of `devtools` from CRAN with `install.packages("devtools")`.

2. Install the release version of `RSQLite` from CRAN with `install.packages("RSQLite")`.

3. Install `CigSsa.db` from github using:
```R
library(devtools)
devtools::install_git('https://gitlab.com/Cigene/CigSsa.db.git')
```

## Tips

 1. Functions (quick description), R help pages are available for all functions (use `?get.genes` for help on the get.genes function). Almost all of the functions expect CIGENE gene identifiers (like CIGSSA\_052606). 
	 * **get.genes**: Returns gene names and more
	 * **get.GO**: Returns Gene Ontlogy
	 * **get.PFAM**: Returns PFAM
	 * **get.KEGG**: Returns KEGG
	 * **get.gtf**: Returns position on the genome
	 * **get.int**: Returns IDs of all genes within a given genomic interval

---

## Gene naming

Gene names were assigned in a conservative way. Since the process of
finding a name for a given gene sequence is a purely automated
process, we tried not to propagate misinformation.


### Pipeline

Predicted protein sequences were [blast](http://blast.ncbi.nlm.nih.gov)ed (`blastp: -evalue 1e-5,
-max_target_seqs 20`) against:

* [UniProt/Swiss-Prot](https://en.wikipedia.org/wiki/UniProt#UniProtKB.2FSwiss-Prot) protein database (manually annotated and reviewed sequences)
* [ensembl](http://www.ensembl.org/index.html) protein sequences (automatically annotated) from the
  following organisms:
1. Astyanax_mexicanus.AstMex102.pep.all.fa 23698
2. Danio_rerio.GRCz10.pep.all.fa 44487
3. Gadus_morhua.gadMor1.pep.all.fa 22100
4. Gasterosteus_aculeatus.BROADS1.pep.all.fa 27576
5. Lepisosteus_oculatus.LepOcu1.pep.all.fa 22483
6. Oryzias_latipes.MEDAKA1.pep.all.fa 24674
7. Poecilia_formosa.PoeFor_5.1.2.pep.all.fa 30898
8. Takifugu_rubripes.FUGU4.pep.all.fa 47841
9. Tetraodon_nigroviridis.TETRAODON8.pep.all.fa 23118
10. Xiphophorus_maculatus.Xipmac4.4.2.pep.all.fa 20454

Informative blastp hits were preferred. Blast hits were considered to
be informative based on:

* Evidence for the existence of the protein (UniProtKB), ranked in the
  order (information from ensembl was crosslinked to the UniProtKB) DB
  in order to retrieve information regarding protein existance. Incase
  no UniProtKB entry could be found for the ensembl hit, protein
  evidence was set to:  4. Protein predicted):
  1. Experimental evidence at protein level
  2. Experimental evidence at transcript level
  3. Protein inferred from homology
  4. Protein predicted
  5. Protein uncertain
* Avoiding hits where the name contained any of the following words:
  * Uncharacterized protein
  * genomic scaffold

### Gene classes

Depending on the quality of the annotation, gene names are divided into
5 different categories (seperated by a '|' from the gene name):

1. **conserved** The following conditions must be fulfilled:
   * The length of the alignment must cover at least 70% of the
     subject length.
   * Percentage of identical matches (pident) must be greater than
     50%
   * The hit sequence must be classified as
       1. **Experimental evidence at protein level** in the UniProtKB db.
   * The hit name must not contain any of the following words:
	   * Uncharacterized protein
	   * genomic scaffold
	   * like
	   * whole genome shotgun sequence
2. **conserved hypothetical** The following conditions must be
   fulfilled:
   * The length of the alignment must cover at least 70% of the
	 subject length.   
3. **hypothetical**
   * The length of the alignment must cover between 20% and 70% of the
	 subject length.
4. **hypothetical, waekly similar**
   * The length of the alignment covers less than 20% of the
	 subject length.
5. **predicted protein** Sequences that do not fulfill any of the
	 criteria above.

### Summary of automated gene naming



Gene class                      | Count
-------------------|------------
                    conserved | 17158
       conserved hypothetical | 8013
                 hypothetical | 19242
 hypothetical, weakly similar | 7057
            predicted protein | 3182

Organism         | Count
------------|-----------
                      swiss| 25241
         drerio_gene_ensembl | 15046
     amexicanus_gene_ensembl |   3576
       pformosa_gene_ensembl | 2156
       olatipes_gene_ensembl | 2043
    xmaculatus_gene_ensembl | 1728
      loculatus_gene_ensembl | 1726
     trubripes_gene_ensembl | 1529
     gaculeatus_gene_ensembl | 1212
 tnigroviridis_gene_ensembl |  229

Name type                     | Count
-------------------|-----------
Uncharacterized protein | 12526
Named protein | 42145

---
## GO annotation

GO annotations were obtained by blasting the predicted coding
sequences against the Swiss-Prot protein DB (using
<cite>[Blast2GO][1]<cite> with the default settings).

GO Term  | Number of genes annotated
--------|---------------------
Biological Process |	35417		
Molecular Function | 33687
Cellular component | 34556

---
## KEGG annotation

KEGG annotations were fetched using <cite>[KAAS][2]</cite>.


---
## References

[1] Conesa, A., Gotz, S., Garcia-Gomez, J. M., Terol, J., Talon, M., &
Robles, M. (2005). Blast2GO: a universal tool for annotation,
visualization and analysis in functional genomics
research. Bioinformatics, 21(18), 3674–3676.

[2] Moriya, Y., Itoh, M., Okuda, S., Yoshizawa, A. C., & Kanehisa,
M. (2007). KAAS: an automatic genome annotation and pathway
reconstruction server. Nucleic Acids Research, 35(Web Server),
W182–W185. 
